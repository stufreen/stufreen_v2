---
title: Stu Freen | Creative Developer
author: Stu Freen
description: Toronto-based web developer focussed on building great user experiences.
layout: page.html
priority: 1
date: 2017-10-22
---
A jack of all trades. Stu left his job as a lawyer at a Bay Street law firm in 2015 to follow his passion for interactive design. His undergrad degree is in Computer Science, but his interests lie at the intersection of design, development and user experience. JavaScript Developer at [Rangle.io](http://www.rangle.io).

Stu is a full-stack dev. His preferred tech stack involves React, Node and Express.