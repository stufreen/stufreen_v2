---
title: CFlora
image: cflora-med.jpg
thumbnail: cflora-thumb.jpg
bigThumb: cflora-med.jpg
shortDescription: Website
link: http://www.cflora.ca
priority: 10
---
Design and coding of a website for a floral arranger. Custom features include a masonry-style photo stream and touch sensitive image gallery. [(Link)](http://www.cflora.ca)