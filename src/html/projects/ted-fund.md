---
title: Ted Rogers Scholarship Fund
image: ted-fund-med.jpg
thumbnail: ted-fund-thumb.jpg
bigThumb: ted-fund-med.jpg
shortDescription: Website
link: http://www.tedrogersfund.com
priority: 13
---
Coding of interactive website for Rogers scholarship project. Features include a Mapbox-powered locator tool, multiple video players, animations and social media scraper. [(Link)](http://www.tedrogersfund.com)