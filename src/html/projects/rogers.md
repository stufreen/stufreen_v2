---
title: Rogers History
image: rogers-history-med.jpg
thumbnail: rogers-history-thumb.jpg
bigThumb: rogers-history-med.jpg
shortDescription: Website
link: http://about.rogers.com/who-we-are/our-history/
priority: 11
---
Coding of animated company history for large media company. Extensive use of animated SVGs and the Greensock JavaScript library. Project with Pilot Interactive. [(Link)](http://about.rogers.com/who-we-are/our-history/)