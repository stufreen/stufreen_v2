---
title: Law Scout
image: lawscout-med.jpg
thumbnail: lawscout-thumb.jpg
bigThumb: lawscout-med.jpg
shortDescription: Website
link: http://www.lawscout.ca/promo
priority: 9
---
Design and coding of a landing page based on client's existing design language. [(Link)](http://www.lawscout.ca/promo)