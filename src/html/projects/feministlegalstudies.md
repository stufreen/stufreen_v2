---
title: Institute for Feminist Legal Studies
image: ifls.jpg
thumbnail: ifls-thumb.jpg
bigThumb: ifls-med.jpg
shortDescription: Print poster
priority: 5
---
Design of a print poster for the Institute for Feminist Legal Studies. Challenges included presenting large blocks of text and several institutional logos while maintaining a visual style.