---
title: Construction project
image: book.jpg
thumbnail: book-thumb.jpg
bigThumb: book-med.jpg
shortDescription: Book design
priority: 7
---
Worked with a group of investors to create a polished, beautiful application for a construction project. From cover design to page layout to binding, no detail was overlooked.